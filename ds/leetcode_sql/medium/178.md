# 思路
子查询需要关注查出来的一般是值(好做比较)

![](../img/截屏2023-07-27%2015.10.28.png)
## 代码
```
select a.score as Score,
(select count(distinct b.score) from Scores b where b.score >= a.score) as `Rank`
from Scores a
order by a.score desc
```